#ifndef PXLATN_MATRIX
#define PXLATN_MATRIX

#include <algorithm>
#include <array>
#include <cmath>
#include <stdexcept>

namespace pxlatn {


/// \namespace array_utils
/// \brief Various actions on arrays
namespace array_utils {

	/// \cond impl
	template< typename _Tp, std::size_t _Nm, typename Function, std::size_t... I >
	constexpr std::array<_Tp,_Nm> transform_impl(
			std::array<_Tp,_Nm> const & lhs,
			_Tp const & rhs,
			Function func, std::index_sequence<I...> /*indices*/
			){
		return {{ func( lhs[I], rhs )... }};
	}
	/// \endcond

	/// \brief Run `func` on each element of array with const
	///
	/// \param lhs array of length `_Nm`
	/// \param rhs constant of type `_Tp`
	/// \param func any callable taking two `_Tp` parameters
	///
	/// \details Run `func( lhs[i], rhs )` for each `i` in [0, _Nm)
	template< typename _Tp, std::size_t _Nm, class Function >
	constexpr std::array<_Tp,_Nm> transform(
			std::array<_Tp,_Nm> const & lhs,
			_Tp const & rhs,
			Function func
			){
		return transform_impl( lhs, rhs, func, std::make_index_sequence<_Nm>() );
	}

	/// \cond impl
	template< typename _Tp, std::size_t _Nm, typename Function, std::size_t... I >
	constexpr std::array<_Tp,_Nm> transform_impl(
			std::array<_Tp,_Nm> const & lhs,
			std::array<_Tp,_Nm> const & rhs,
			Function func, std::index_sequence<I...> /*indices*/
			){
		return {{ func( lhs[I], rhs[I] )... }};
	}
	/// \endcond

	/// \brief Run `func` on each pair of elements of input arrays
	///
	/// \param lhs array of length `_Nm`
	/// \param rhs array of length `_Nm`
	/// \param func any callable taking two `_Tp` parameters
	///
	/// \details Run `func( lhs[i], rhs[i] )` for each `i` in [0, _Nm)
	template< typename _Tp, std::size_t _Nm, class Function >
	constexpr std::array<_Tp,_Nm> transform(
			std::array<_Tp,_Nm> const & lhs,
			std::array<_Tp,_Nm> const & rhs,
			Function func
			){
		return transform_impl( lhs, rhs, func, std::make_index_sequence<_Nm>() );
	}

	/// \brief Calculate the dot or scalar product of two vector arrays
	///
	/// \details Calculates \f$ lhs &sdot; rhs \f$
	template<typename Scalar, std::size_t Size>
	constexpr Scalar dot(
		std::array<Scalar, Size> const & lhs,
		std::array<Scalar, Size> const & rhs
	){
		Scalar result{};
		for( std::size_t i{ 0 }; i < Size; ++i ) {
			result += lhs[i] * rhs[i];
		}
		return result;
	}

} // namespace array_utils

/// \class Matrix
/// \brief %Matrix with hard-coded dimensions
///
/// \tparam _Tp Scalar type of matrix entries
/// \tparam _Nrows %Matrix rows
/// \tparam _Ncols %Matrix cols
///
/// \note Most methods are `constexpr`, and therefore can be calculated at compile time
template< typename _Tp, std::size_t _Nrows, std::size_t _Ncols=1 >
class Matrix {
	public:
	// intrinsic properties of type

		/// \brief check for row or column matrix based vector
		constexpr static bool is_vector = _Ncols == 1 || _Nrows == 1;

		constexpr static size_t dimensions = is_vector ? ( _Ncols > _Nrows ? _Ncols : _Nrows) : 0;

		/// \brief check for square matrix
		constexpr static bool is_square = _Ncols == _Nrows;

		/// \brief typedef of scalar type
		using scalar = _Tp;

		/// \brief rows and columns from template parameters
		constexpr static std::size_t rows = _Nrows, columns = _Ncols;

	private:
		/// \brief Number of cells, and size of elements array
		constexpr static std::size_t cells{ _Nrows * _Ncols };

		/// \brief `std::array` matrix element data container
		std::array< _Tp, cells > data;

	// implementations

		/// \cond impl
		template< std::size_t... I >
		constexpr Matrix< _Tp, _Ncols, _Nrows >
		transpose_impl( std::index_sequence<I...> /*indices*/ ) const {
			return {{
				data[
					// return YX[I] for each in XY[]
					(( I % _Nrows ) * _Ncols ) + ( I / _Nrows )
				]...
			}};
		}
		/// \endcond

		/// \cond impl
		template< std::size_t... I >
		constexpr std::array< _Tp, _Nrows >
		get_col_impl( std::size_t col, std::index_sequence<I...> /*indices*/ ) const {
			return {{ data.at( ( I * _Ncols ) + col )... }};
		}
		/// \endcond

		/// \cond impl
		template< std::size_t... I >
		constexpr std::array< _Tp, _Ncols >
		get_row_impl( std::size_t row, std::index_sequence<I...> /*indices*/ ) const {
			return {{ data.at( ( row * _Ncols ) + I )... }};
		}
		/// \endcond

		//	< Rr = Ar, Rc = Bc, Ac = Br >
		/// \cond impl
		template < std::size_t B_cols, std::size_t... I > // B_rows == A_cols
		constexpr Matrix< _Tp, _Nrows, B_cols >
		matrix_multiply_impl(
			Matrix< _Tp, _Ncols, B_cols > const & rhs,
			std::index_sequence<I...> /*indices*/ = std::make_index_sequence< _Nrows * B_cols >()
		) const {
			return {{
				array_utils::dot(
						get_row( I / B_cols ),	// Y
					rhs.get_col( I % B_cols )	// X
				)...
			}};
		}
		/// \endcond

	public:
		constexpr scalar & x(){ static_assert( is_vector && dimensions >= 1 ); return data[0]; }
		constexpr scalar & y(){ static_assert( is_vector && dimensions >= 2 ); return data[1]; }
		constexpr scalar & z(){ static_assert( is_vector && dimensions >= 3 ); return data[2]; }
		constexpr scalar const & x() const { static_assert( is_vector && dimensions >= 1 ); return data[0]; }
		constexpr scalar const & y() const { static_assert( is_vector && dimensions >= 2 ); return data[1]; }
		constexpr scalar const & z() const { static_assert( is_vector && dimensions >= 3 ); return data[2]; }

		/// \brief initializer list constructor
		///
		/// \details Allows construction of a matrix by passing the parameter pack `args` to the `data` `std::array` member
		/// \code
		///		Matrix< int, 2, 3 >
		///			matrix {
		///				1, 2, 3,
		///				4, 5, 6
		///			};
		/// \endcode
		/// Matrix can also be constructed by moving a `std::array`
		/// (doxygen isn't letting me document multiple constructors)
		template<typename... Types>
		constexpr Matrix( Types... args )
			: data{ { static_cast<_Tp>(args)... } } {}

		/// \brief construct matrix by moving in array
		///
		/// \todo Can I not document multiple constructors?
		constexpr Matrix( std::array<_Tp, cells> && arr ) : data{ arr } {}
		constexpr Matrix( std::array<_Tp, cells> & arr ) : data{ arr } {}

		/// \brief Return identity matrix
		///
		/// \returns A matrix, with ones on the main diagonal and zeros elsewhere.
		static constexpr Matrix identity() {
			Matrix matrix{};
			for( size_t i = 0; i < std::min( _Nrows, _Ncols ); ++i ) {
				matrix.get( i, i ) = 1;
			}
			return matrix;
		}

		/// \brief Return permutation matrix for a given ordering of indices
		///
		/// \details A permutation matrix is a square binary matrix that has exactly one one in each row and column and zeroes elsewhere.
		/// This function returns a valid permutation matrix so long as the indices list is comprised of unique and valid indices
		static constexpr Matrix permutation(
			std::array<size_t,rows> const & indices
		){
			static_assert( is_square, "Permutation matrix currently only supports square matrices.");
			Matrix matrix{};
			for( size_t i{0}; i<rows ; ++i ){
				matrix.get(indices[i],i) = 1;
			}
			return matrix;
		}

		/// \brief Form a 2D rotation on the plane defined by AB
		///
		/// \tparam A Matrix axis
		/// \tparam B Matrix axis
		/// \param theta Angle &theta; to rotate by (defined in radians)
		/// \return rotation Matrix
		///
		/// \details Given plane of rotation AB and angle theta, produces a rotation matrix to rotate A in direction B
		/// Source: <a href="https://en.wikipedia.org/wiki/Rotation_matrix">Wikipedia - Rotation matrix</a>
		/// Source: <a href="https://en.wikipedia.org/wiki/Plane_of_rotation">Wikipedia - Plane of rotation</a>
		/// \notes min(columns,rows) is to handle non-square matrices to be used for rotation
		template< size_t A, size_t B >
		static Matrix rotate_plane( _Tp theta ) {
			static_assert( A != B, "Two separate dimensions must be specified for rotation" );
			static_assert( A < std::min( columns, rows ), "A too large" );
			static_assert( B < std::min( columns, rows ), "B too large" );

			auto rotation = identity();
			_Tp const cosine = std::cos( theta );
			_Tp const sine = std::sin( theta );

			rotation.get( A, A ) = cosine;
			rotation.get( B, A ) = -sine;
			rotation.get( B, B ) = cosine;
			rotation.get( A, B ) = sine;
			return rotation;
		}

		/// \brief return a submatrix by passing a sequence of indices for rows and columns to include
		///
		/// \param row_i index_sequence of row indices to include
		/// \param col_i index_sequence of column indices to include
		template<std::size_t rows_s, std::size_t cols_s>
		constexpr Matrix<_Tp, rows_s, cols_s> submatrix(
			std::array<std::size_t, rows_s> rows,
			std::array<std::size_t, cols_s> cols
		) const {
			std::array<_Tp, rows_s * cols_s> result;
			std::size_t i{};
			for( _Tp const & y : rows ){
				for( _Tp const & x : cols ){
					result[i++] = get( x, y );
				}
			}
			return Matrix<_Tp, rows_s, cols_s>{ std::move(result) };
		}

		constexpr _Tp determinant() const {
			static_assert( is_square, "Determinant calculation is only implemented for a square matrix.");
			if constexpr ( cells == 0 ){
				return 0;
			}
			if constexpr ( cells == 1 ){
				return get(0,0);
			}
			if constexpr ( cells > 1 ){
				_Tp result{};
				bool sign = true;
				for( size_t col = 0; col < _Ncols ; ++col ){
					std::array<_Tp, ( _Nrows - 1 ) * ( _Ncols - 1 )> arr{};
					auto arr_i = arr.begin();
					for( size_t y = 1; y < _Nrows; ++y ) {
						for( size_t x = 0; x < _Ncols; ++x ) {
							if( x == col ) { continue; }
							*arr_i++ = get(x,y);
						}
					}
					Matrix<_Tp, _Nrows - 1, _Ncols - 1> sub{ std::move(arr) };
					_Tp val = get(col,0) * sub.determinant();
					if( sign ){
						result += val;
					} else {
						result -= val;
					}
					sign = !sign;
				}
				return result;
			}
		}

		//	special members:
		//	 - default constructor		Matrix()
		//	 - destructor				~Matrix()
		//	 - copy constructor			Matrix( Matrix const & )
		//	 - copy assignment			Matrix& operator=( Matrix const & )
		//	 - move constructor			Matrix( Matrix&& )
		//	 - move assignment			Matrix& operator=( Matrix&& )

		/// \brief Access the elements of the `data` array
		constexpr _Tp& operator[] ( size_t pos ){
			return data.at( pos );
		}

		/// \brief Access the elements of the `data` array
		constexpr const _Tp& operator[] ( size_t pos ) const {
			return data.at( pos );
		}

		/// \brief Access matrix elements by coordinates
		constexpr _Tp& get ( std::size_t x, std::size_t y ){
			return data[ (_Ncols*y)+x ];
		}

		/// \brief Access matrix elements by coordinates
		constexpr const _Tp& get ( std::size_t x, std::size_t y ) const {
			return data[ (_Ncols*y)+x ];
		}

		/// \brief For each `[i]` in the return array, work out the `[i]` of the local data array
		///
		/// \note Maps element `return[i]` to coords `[x,y]`, flips that to `[y,x]` and maps that to `data[o]`.
		/// \note Remember that the cols and rows are flipped on the return
		constexpr Matrix< _Tp, _Ncols, _Nrows > transpose() const {
			return transpose_impl( std::make_index_sequence< cells >() );
		}

		/// \brief Calculate the square of the length of Vector
		constexpr _Tp length_sq() const {
			static_assert( is_vector, "Length only makes sense in the context of a vector." );
			return array_utils::dot( data, data );
		}

		/// \brief Calculate the length of Vector
		///
		/// \note Not `constexpr` because `std::sqrt` is not marked `constexpr` in the standard
		_Tp length() const {
			static_assert( is_vector, "Length only makes sense in the context of a vector." );
			return std::sqrt( length_sq() );
		}

		/// \brief Normalize Vector (resize vector to length 1)
		///
		/// \details
		/// \f$ \mathbf{\hat{u}} = \frac{\mathbf{u}}{\|\mathbf{u}\|} \f$
		/// \note Not `constexpr` because `std::sqrt` is not marked `constexpr` in the standard
		void normalise(){
			static_assert( is_vector, "conversion to unit vector is only implemented for vectors." );
			data = array_utils::transform( data, length(), [](_Tp L, _Tp R){ return L/R; } );
		}

		void normalize(){
			normalise();
		}

		/// \brief Unit Vector (resize vector to length 1)
		///
		/// \return Unit vector in the direction of `this`
		/// \details
		/// \f$ \mathbf{\hat{u}} = \frac{\mathbf{u}}{\|\mathbf{u}\|} \f$
		/// \note Not `constexpr` because `std::sqrt` is not marked `constexpr` in the standard
		Matrix unit() const {
			static_assert( is_vector, "conversion to unit vector is only implemented for vectors." );
			return { array_utils::transform( data, length(), [](_Tp L, _Tp R){ return L/R; } ) };
		}

		/// \brief Resize Vector to length `max`
		///
		/// \param max Size to which this Vector should be resized
		/// \details `Vector *= max / Vector.length`
		/// \note Not `constexpr` because `std::sqrt` is not marked `constexpr` in the standard
		void truncate( _Tp max ){
			static_assert( is_vector, "Length only makes sense in the context of a vector." );
			data = array_utils::transform( data, max / length(), [](_Tp L, _Tp R){ return L*R; } );
		}

		/// \brief Returns The Vector opposite to `this`
		constexpr Matrix invert() const {
			static_assert( is_vector, "invert() only implemented for vectors." );
			return *this * -1;
		}

		/* Others (recommended by book)
		constexpr int sign( Matrix const & param );
		constexpr _Tp distance_sq( Matrix const & param );
		constexpr _Tp distance( Matrix const & param );
		*/

		/// \brief Entrywise sum of `this` with `rhs`
		constexpr void operator+=( Matrix const & rhs ){
			data = array_utils::transform( data, rhs.data, [](_Tp L, _Tp R){ return L+R; } );
		}

		/// \brief Entrywise difference of `this` with `rhs`
		constexpr void operator-=( Matrix const & rhs ){
			data = array_utils::transform( data, rhs.data, [](_Tp L, _Tp R){ return L-R; } );
		}

		/// \brief Scalar multiplication of `this` by `rhs`
		constexpr void operator*=( _Tp const & rhs ){
			data = array_utils::transform( data, rhs, [](_Tp L, _Tp R){ return L*R; } );
		}

		/// \brief Scalar division of `this` by `rhs`
		constexpr void operator/=( _Tp const & rhs ){
			if( rhs == 0 ){
				throw std::overflow_error("Divide by zero exception");
			}
			data = array_utils::transform( data, rhs, [](_Tp L, _Tp R){ return L/R; } );
		}

		/// \brief Entrywise sum of two matrices
		constexpr Matrix operator+( Matrix const & rhs ) const {
			return { array_utils::transform( data, rhs.data, [](_Tp L, _Tp R){ return L+R; } ) };
		}

		/// \brief Entrywise difference of two matrices
		constexpr Matrix operator-( Matrix const & rhs ) const {
			return { array_utils::transform( data, rhs.data, [](_Tp L, _Tp R){ return L-R; } ) };
		}

		/// \brief Negative of matrix
		constexpr Matrix operator-() const {
			return { array_utils::transform( data, data, [](_Tp n, _Tp /*unused*/){ return -n; } ) };
		}

		/// \brief Multiplication of matrix by a scalar
		constexpr Matrix operator*( _Tp const & rhs ) const {
			return { array_utils::transform( data, rhs, [](_Tp L, _Tp R){ return L*R; } ) };
		}

		friend constexpr Matrix operator*( _Tp const & lhs, Matrix const & rhs ){
			return rhs * lhs;
		}

		/// \brief Division of matrix by a scalar
		constexpr Matrix operator/( _Tp const & rhs ) const {
			if( rhs == 0 ){
				throw std::overflow_error("Divide by zero exception");
			}
			return { array_utils::transform( data, rhs, [](_Tp L, _Tp R){ return L/R; } ) };
		}

		/// \brief Check for equality
		/// \note Not the nicest implementation, but constexpr and correct enough
		/// \note Equality of floating point types is always dodgy anyway
		constexpr bool operator==( Matrix const & rhs ) const {
			for( std::size_t iter = 0 ; iter < cells ; ++iter ){
				const auto & L = data[iter];
				const auto & R = rhs.data[iter];
				if constexpr ( std::is_integral<_Tp>::value ){
					if( L != R ){
						return false;
					}
				}
				if constexpr ( std::is_floating_point<_Tp>::value ){
					if(
						std::abs(L - R) > (
							std::numeric_limits<_Tp>::epsilon() *
							std::max(
								std::max( std::abs(L), std::abs(R) ), _Tp{1.0}
							)
						)
					){
						return false;
					}
				}
			}
			return true;
		}

		/// \brief Inversion of \link operator== \endlink
		constexpr bool operator!=( Matrix const & rhs ) const {
			return !( *this == rhs );
		}

		/// \brief Calculate the dot or scalar product of two Vectors
		///
		/// \details
		/// Calculates `lhs &sdot; rhs`
		/// In essence: sums the entrywise multiplication of the two matrices
		/// \f[ \mathbf{a}\cdot\mathbf{b}=\sum_{i=1}^n a_ib_i=a_1b_1+a_2b_2+\cdots+a_nb_n \f]
		///	\f[ \mathbf{a}\cdot\mathbf{b}=\|\mathbf{a}\|\ \|\mathbf{b}\|\cos(\theta) \f]
		/// \f[ \theta=\frac{\arccos(\mathbf{a}\cdot\mathbf{b})}{\|\mathbf{a}\|\times\|\mathbf{b}\|} \f]
		friend constexpr _Tp dot( Matrix<_Tp,_Nrows,_Ncols> const & lhs, Matrix<_Tp,_Nrows,_Ncols> const & rhs ){
			static_assert( Matrix<_Tp,_Nrows,_Ncols>::is_vector, "A scalar product between Matrices is not defined");
			return array_utils::dot( lhs.data, rhs.data );
		}

		/// \brief Get a single column of the Matrix as an array
		constexpr std::array< _Tp, _Nrows >
		get_col( std::size_t col ) const {
			return get_col_impl( col, std::make_index_sequence< _Nrows >() );
		}

		/// \brief Get a single row of the Matrix as an array
		constexpr std::array< _Tp, _Ncols >
		get_row( std::size_t row ) const {
			return get_row_impl( row, std::make_index_sequence< _Ncols >() );
		}

		// < Rr = Ar, Rc = Bc, Ac = Br >
		/// \brief Calculates the matrix product of two \link Matrix Matrices \endlink
		///
		/// \details
		///  If Matrix \f$ A \f$ is \f$ u \times v \f$
		/// and Matrix \f$ B \f$ is \f$ v \times w \f$
		/// Then \f$ AB \f$ would be \f$ u \times w \f$
		/// and \f$ BA \f$ would be undefined unless \f$ u = w \f$,
		template < std::size_t B_cols >
		constexpr Matrix< _Tp, _Nrows, B_cols >
		operator*( Matrix< _Tp, _Ncols, B_cols > const & rhs ) const {
			return matrix_multiply_impl( rhs,
				std::make_index_sequence< _Nrows * B_cols >()
			);
		}

		// < Rr = Br, Rc = Ac, Bc = Ar >
		/// \brief Convenience operator allowing for left-to-right chaining of matrices
		///
		/// \details
		///  `v | A | B | C` is equivalent to `C * B * A * v`
		template <std::size_t B_rows>
		constexpr Matrix< _Tp, B_rows, _Ncols >
		operator|( const Matrix< _Tp, B_rows, _Nrows > & rhs ) const {
			return rhs * *this;
		}
};

/// \class Vector
/// \brief Vector is an alias for a columnar Matrix
///
/// \tparam _Tp Scalar type of matrix entries
/// \tparam _Nm %Vector cells
template< typename _Tp, std::size_t _Nm >
using Vector = Matrix< _Tp, _Nm, 1UL >;

/// \relates Vector
/// \brief Apply a Matrix to a Vector
///
/// \details basically `V = M * V`
template< typename _Tp, size_t _dim >
constexpr void operator|=( Vector< _Tp, _dim > & V, Matrix< _Tp, _dim,_dim > const & M ){
	V = M * V;
}

} // namespace pxlatn

#endif // PXLATN_MATRIX

// vim: filetype=cpp.doxygen
