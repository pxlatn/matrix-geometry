#ifndef PXLATN_MATRIX_4D
#define PXLATN_MATRIX_4D

#include "Matrix.hpp"
#include <cmath>  // std::sin, std::cos

namespace pxlatn {

template<typename _Tp>
auto rotate_xz( _Tp theta ) {
	return Matrix<_Tp, 4, 4>::template rotate_plane<0, 2>( theta );
}

template<typename _Tp>
auto rotate_xy( _Tp theta ) {
	return Matrix<_Tp, 4, 4>::template rotate_plane<0, 1>( theta );
}

template<typename _Tp>
auto rotate_yz( _Tp theta ) {
	return Matrix<_Tp, 4, 4>::template rotate_plane<1, 2>( theta );
}

template<typename _Tp>
auto rotate_xw( _Tp theta ) {
	return Matrix<_Tp, 4, 4>::template rotate_plane<0, 3>( theta );
}

template<typename _Tp>
auto rotate_yw( _Tp theta ) {
	return Matrix<_Tp, 4, 4>::template rotate_plane<1, 3>( theta );
}

template<typename _Tp>
auto rotate_zw( _Tp theta ) {
	return Matrix<_Tp, 4, 4>::template rotate_plane<2, 3>( theta );
}

} // namespace pxlatn

#endif // PXLATN_MATRIX_3D

// vim: filetype=cpp.doxygen
