#ifndef PXLATN_MATRIX_3D
#define PXLATN_MATRIX_3D

#include "Matrix.hpp"
#include <cmath>

namespace pxlatn {

/* Application of matrix maths to vectors (particularly 3D)
 *
 * Reference:
 * https://en.wikipedia.org/wiki/Rotation_matrix#In_three_dimensions
 * https://en.wikipedia.org/wiki/Scaling_(geometry)#Using_homogeneous_coordinates
 * https://en.wikipedia.org/wiki/Translation_(geometry)#Matrix_representation
 *
 * https://en.wikipedia.org/wiki/Reflection_(mathematics)#Properties
 * https://en.wikipedia.org/wiki/Shear_mapping
 */

/// \relates Matrix
/// \brief Rotation in 2D
///
/// \param theta Angle &theta; to rotate by (defined in radians)
/// \return 2x2 rotation Matrix
template< typename _Tp >
auto rotate_2D( _Tp theta ){
	return Matrix< _Tp, 2, 2 >::template rotate_plane< 0, 1 >( theta );
}

/// \relates Matrix
/// \brief 3D rotation in the x axis
///
/// \param theta Angle &theta; to rotate by (defined in radians)
/// \return 3x3 rotation Matrix
template< typename _Tp >
auto rotate_x( _Tp theta ){
	return Matrix< _Tp, 3, 3 >::template rotate_plane< 1, 2 >( theta );
}

/// \relates Matrix
/// \brief 3D rotation in the y axis
///
/// \param theta Angle &theta; to rotate by (defined in radians)
/// \return 3x3 rotation Matrix
template< typename _Tp >
auto rotate_y( _Tp theta ){
	return Matrix< _Tp, 3, 3 >::template rotate_plane< 2, 0 >( theta );
}

/// \relates Matrix
/// \brief 3D rotation in the z axis
///
/// \param theta Angle &theta; to rotate by (defined in radians)
/// \return 3x3 rotation Matrix
template< typename _Tp >
auto rotate_z( _Tp theta ){
	return Matrix< _Tp, 3, 3 >::template rotate_plane< 0, 1 >( theta );
}

/// \relates Matrix
/// \brief 3D rotation in an arbitary axis defined by Vector
///
/// \param U unit Vector about which to rotate by &theta;
/// \param theta Angle &theta; to rotate by (defined in radians)
/// \return 3x3 rotation Matrix
///
/// \details
/// Source: https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
/// Source: https://en.wikipedia.org/wiki/Rotation_matrix#Nested_dimensions
template< typename _Tp >
Matrix< _Tp, 3,3 > rotate_a( Vector<_Tp,3> U, _Tp theta ){
	auto const V = U.unit();
	_Tp const c = std::cos( theta );
	_Tp const s = std::sin( theta );
	_Tp const c1 = 1 - c;
	_Tp const x = V[0];
	_Tp const y = V[1];
	_Tp const z = V[2];
	return {
		x * x * c1 + c,        y * x * c1 - z * s,    z * x * c1 + y * s,
		x * y * c1 + z * s,    y * y * c1 + c,        z * y * c1 - x * s,
		x * z * c1 - y * s,    y * z * c1 + x * s,    z * z * c1 + c
	};
}

/// \relates Matrix
/// \brief Cross product of two similar \link Vector Vectors \endlink
///
/// \note Specialised for 3D as a cross product of vectors exists only in 3 and 7 dimensional Euclidean space.
/// \note https://en.wikipedia.org/wiki/Cross_product : "If the product is limited to non-trivial binary products with vector results, it exists only in three and seven dimensions."
///
/// \todo Maybe also specialise for 7D
template< typename _Tp, std::size_t _Nrows, std::size_t _Ncols >
constexpr Matrix<_Tp,_Ncols,_Nrows> cross( Matrix<_Tp,_Nrows,_Ncols> const & lhs, Matrix<_Tp,_Nrows,_Ncols> const & rhs ) = delete;

template< typename _Tp >
constexpr Vector<_Tp,3> cross( Vector<_Tp,3> const & lhs, Vector<_Tp,3> const & rhs ){
	return {
		( lhs[1] * rhs[2] ) - ( lhs[2] * rhs[1] ),
		( lhs[2] * rhs[0] ) - ( lhs[0] * rhs[2] ),
		( lhs[0] * rhs[1] ) - ( lhs[1] * rhs[0] )
	};
}

/// \relates Matrix
/// \brief Skew Symmetric Matrix of Vector
///
/// \details https://en.wikipedia.org/wiki/Skew-symmetric_matrix#Cross_product
template< typename _Tp >
constexpr Matrix< _Tp, 3,3 > skew_symmetric( Vector<_Tp,3> v ){
	_Tp const x = v[0];
	_Tp const y = v[1];
	_Tp const z = v[2];
	return {
		 0,-z, y,
		 z, 0,-x,
		-y, x, 0
	};
}

/// \relates Matrix
/// \brief Calculate the rotation matrix to rotate a vector into another
///
/// \details
/// http://math.stackexchange.com/a/476311
/// \f$ R = I + [v]_{\times} + [v]_{\times}^2\frac{1}{1+c} \f$
template< typename _Tp >
constexpr Matrix< _Tp, 3,3 > rotate_onto( Vector<_Tp,3> a, Vector<_Tp,3> b ){
	auto const c = dot( a, b );
	auto const v = cross( a, b );
	auto const ssc = skew_symmetric(v);
	_Tp const one = 1; // to avoid type narrowing or other warnings
	return Matrix< _Tp, 3, 3 >::identity() + ssc + ( ssc * ssc ) * ( one / ( one + c ) );
}

/// \relates Matrix
/// \brief Angle between two vectors
// simple A.B == ||A||*||B||*cos(theta)
// so theta == acos( A.B / ||A||*||B|| )
template< typename _Tp, size_t _dim >
// constexpr
_Tp angle_between(
	Matrix< _Tp, _dim, 1 > const & A,
	Matrix< _Tp, _dim, 1 > const & B
){
	return std::acos(
		dot( A, B ) /
		(A.length() * B.length())
	);
}

/// \relates Matrix
/// \brief Distance from a point to a line
///
/// \details Line through A and B,
// https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
template< typename _Tp, size_t _dim >
// constexpr
_Tp scatter(
	Matrix< _Tp, _dim, 1 > A,
	Matrix< _Tp, _dim, 1 > B,
	Matrix< _Tp, _dim, 1 > target
){
	target -= A;
	B -= A;
	B.normalize(); // |B| = 1
	_Tp t = dot( B, target ); // hyp*cos() == adj
	return std::sqrt( target.length_sq() - t*t );
}

/// \relates Matrix
/// \brief reflection through the plane orthogonal to `normal`
template< typename _Tp, size_t _dim >
constexpr
Matrix< _Tp, _dim, 1 > reflect(
	Matrix< _Tp, _dim, 1 > const & in,
	Matrix< _Tp, _dim, 1 > const & normal
){
	return in - normal * (
		2 * dot(in, normal)
		/ dot(normal, normal)
	);
}

enum class intersect_type {
	parallel,  /// lines never meet
	collinear, /// two sections of same line
	hit,       /// lines intersect within segments given
	miss       /// lines intersect outside at least one of the segments
};

/// \relates Matrix
/// \brief intersect of two line segments
///
/// \return {} if lines are parallel or collinear, otherwise returns intersect point
/// \note
///		https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
///		https://stackoverflow.com/a/565282
///		cross()[2] is the 2d vector cross product mentioned
///
///		only works if vectors are coplanar
///		https://stackoverflow.com/a/6665533
///		https://en.wikipedia.org/wiki/Determinant
///		https://en.wikipedia.org/wiki/Singular-value_decomposition
///
///		Maybe should return `a` and `b`
template< typename _Tp, size_t _dim=3 >
std::tuple< intersect_type, Matrix< _Tp, _dim, 1 > >
intersect( // arguments in A1->A2,B1->B2 order
	Matrix< _Tp, _dim, 1 > const & A1,
	Matrix< _Tp, _dim, 1 > const & A2,
	Matrix< _Tp, _dim, 1 > const & B1,
	Matrix< _Tp, _dim, 1 > const & B2
){
	auto EQ = [](_Tp L, _Tp R) {
		return std::abs(L - R) <= (
			std::numeric_limits<_Tp>::epsilon() *
			std::max(
				std::max( std::abs(L), std::abs(R) ), _Tp{1.0}
			)
		);
	};
	auto a = cross(B1 - A1, B2)[2] / cross(A2, B2)[2];
	auto b = cross(A1 - B1, A2)[2] / cross(B2, A2)[2];
	// switch to use better float compare - currently basically absent
	if( EQ(cross(A2, B2)[2], _Tp{0.0} ) ){
		if( EQ(cross(B1 - A1, A2)[2], _Tp{0.0} ) ){
			return { intersect_type::collinear, Matrix<_Tp,_dim,1>{} };
		}
		return { intersect_type::parallel, Matrix<_Tp,_dim,1>{} };
	}
	if( a >= 0.0 && a <= 1.0 && b >= 0.0 && b <= 1.0 ){
		// intersect
		// A1+(a*A2) == B1+(b*B2)
		return { intersect_type::hit, A1+(A2*a) };
	}
	// intersect not in sections
	return { intersect_type::miss, A1+(A2*a) };
}

} // namespace pxlatn

#endif // PXLATN_MATRIX_3D

// vim: filetype=cpp.doxygen
