#ifndef GEO_MATRIX_OSTREAM
#define GEO_MATRIX_OSTREAM

#include "Matrix.hpp"
#include <ostream>

/// \brief output Matrix to ostream
template< typename _Tp, std::size_t _Nrows, std::size_t _Ncols >
std::ostream & operator<<( std::ostream & os, pxlatn::Matrix<_Tp,_Nrows,_Ncols> const & rhs ){
	if constexpr ( _Nrows * _Ncols > 0 ){
		os << ' ' << rhs[0];
		for( size_t i = 1 ; i < _Nrows*_Ncols ; ++i ){
			if( i % _Ncols == 0 ){
				os << "\n " << rhs[i];
			} else {
				os << ", " << rhs[i];
			}
		}
	}
	return os;
}

/// \brief output Vector to ostream
template< typename _Tp, std::size_t _Nm >
std::ostream & operator<<( std::ostream & os, pxlatn::Matrix<_Tp,_Nm,1> const & rhs ){
	if constexpr ( _Nm > 0 ){
		os << ' ' << rhs[0];
		for( size_t i = 1 ; i < _Nm ; ++i ){
			os << ", " << rhs[i];
		}
	}
	return os;
}

#endif // GEO_MATRIX_OSTREAM
