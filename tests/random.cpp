#include "Matrix.hpp"
#include "Matrix3D.hpp"
#include "Matrix_ostream.hpp"
#include "gtest.hpp"
#include <cstdlib>  // std::getenv
#include <random>

/* Tests of random input to check output */

using namespace pxlatn;
using rand_type = uint8_t;

class random : public ::testing::Test {
	protected:
		unsigned seed{};
		std::default_random_engine engine;
		std::mt19937 gen;
		std::uniform_int_distribution< rand_type > input_dist;

		random(){
			char * ENV_seed = std::getenv("SEED");
			if( ENV_seed != nullptr ){
				seed = std::strtoull( ENV_seed, nullptr, 0 );
			} else {
				seed = std::random_device()();
			}
			std::cout << "Seed: " << seed << '\n';
			engine = std::default_random_engine{ seed };
			gen    = std::mt19937{ engine() };
		}

		template< typename Type, size_t row, size_t col >
		void test_equivalence( Matrix< Type, row, col > a, Matrix< Type, row, col > b ) {
			for( size_t y = 0; y < col; ++y ) {
				for( size_t x = 0; x < row; ++x ) {
					EXPECT_FLOAT_EQ( a.get( x, y ), b.get( x, y ) )
						<< "a:\n" << a << "\nb:\n" << b;
				}
			}
		};
};

// Test if matrixA * matrixB == cellwise multiplication done in loops
TEST_F( random, matrix_mul ){
	Matrix<int64_t,4,4>
		matrix_A, matrix_B,
		matrix_C, matrix_D;
	for( size_t y=0 ; y<4 ; ++y ){
		for( size_t x=0 ; x<4 ; ++x ){
			matrix_A.get( x, y ) = input_dist( gen );
			matrix_B.get( x, y ) = input_dist( gen );
		}
	}
	for( size_t row = 0 ; row < 4 ; ++row ){
		for( size_t col = 0 ; col < 4 ; ++col ){
			int64_t valC = 0;
			int64_t valD = 0;
			for( size_t i = 0 ; i < 4 ; ++i ){
				valC += matrix_A.get( i, row ) * matrix_B.get( col, i );
				valD += matrix_B.get( i, row ) * matrix_A.get( col, i );
			}
			matrix_C.get( col, row ) = valC;
			matrix_D.get( col, row ) = valD;
		}
	}
	gEXPECT_EQ( matrix_A * matrix_B, matrix_C );
	gEXPECT_EQ( matrix_B * matrix_A, matrix_D );
}

TEST_F( random, DISABLED_vector_dot ){
	ADD_FAILURE() << "Test not implemented";
}

// will fail until eq() is improved
TEST_F( random, rotate_onto ){
	auto A = Vector<double,3>{ input_dist( gen ), input_dist( gen ), input_dist( gen ) }.unit();
	auto B = Vector<double,3>{ input_dist( gen ), input_dist( gen ), input_dist( gen ) }.unit();
	auto M = rotate_onto( A, B );
	auto M_t = M.transpose();
	auto oB = M   * A;
	auto oA = M_t * B;
	test_equivalence( B, oB );
	test_equivalence( A, oA );
}

TEST_F( random, determinant3 ){
	int
		a = input_dist(gen), b = input_dist(gen), c = input_dist(gen),
		d = input_dist(gen), e = input_dist(gen), f = input_dist(gen),
		g = input_dist(gen), h = input_dist(gen), i = input_dist(gen);
	Matrix<int, 3, 3> M{
		a, b, c,
		d, e, f,
		g, h, i
	};
	int target = a*e*i - a*f*h + b*f*g - b*d*i + c*d*h - c*e*g;
	auto calc = M.determinant();
	gEXPECT_EQ( calc, target ) << M;
}

TEST_F( random, determinant4 ){
	int
		a = input_dist(gen), b = input_dist(gen), c = input_dist(gen), d = input_dist(gen),
		e = input_dist(gen), f = input_dist(gen), g = input_dist(gen), h = input_dist(gen),
		i = input_dist(gen), j = input_dist(gen), k = input_dist(gen), l = input_dist(gen),
		m = input_dist(gen), n = input_dist(gen), o = input_dist(gen), p = input_dist(gen);
	Matrix<int, 4, 4> M{
		a, b, c, d,
		e, f, g, h,
		i, j, k, l,
		m, n, o, p
	};
	int target = 0
		+a*f*k*p -a*f*l*o
		+a*g*l*n -a*g*j*p
		+a*h*j*o -a*h*k*n

		-b*e*k*p +b*e*l*o
		-b*g*l*m +b*g*i*p
		-b*h*i*o +b*h*k*m

		+c*e*j*p -c*e*l*n
		+c*f*l*m -c*f*i*p
		+c*h*i*n -c*h*j*m

		-d*e*j*o +d*e*k*n
		-d*f*k*m +d*f*i*o
		-d*g*i*n +d*g*j*m
	;
	auto calc = M.determinant();
	gEXPECT_EQ( calc, target ) << M;
}

// vim: filetype=cpp.doxygen
