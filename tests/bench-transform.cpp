#include "Matrix3D.hpp"
#include <benchmark/benchmark.h>
#include <array>
#include <random>

using namespace pxlatn;

// generate array filled with items [start, end)
template< typename T, size_t S >
constexpr std::array<T,S>
range( T start=0, T end=S, T step=1 ){
	std::array<T,S> result{};
	T count = start;
	for( size_t i{0} ; i < S && count != end ; ++i ){
		result[i] = count;
		count += step;
	}
	return result;
}

// get random value for integral type
template<typename T, std::enable_if_t<std::is_integral<T>::value, int> = 0>
T get_rng() {
	static unsigned seed = std::random_device()();
	static std::default_random_engine engine{ seed };
	static std::mt19937 gen{ engine() };
	static std::uniform_int_distribution<T> dist{};
	return dist( gen );
}

// get random value for floating type
template<typename T, std::enable_if_t<std::is_floating_point<T>::value, int> = 0>
T get_rng() {
	static unsigned seed = std::random_device()();
	static std::default_random_engine engine{ seed };
	static std::mt19937 gen{ engine() };
	static std::uniform_real_distribution<T> dist{};
	return dist( gen );
}

// generate array filled with random values
template<typename T, size_t S>
std::array<T,S> generate(){
	std::array<T,S> result{};
	for( T & c : result ){
		c = get_rng<T>();
	}
	return result;
}

template< typename T, size_t S, typename Function >
constexpr std::array<T,S>
transform_loop( std::array<T,S> const & A, std::array<T,S> const & B, Function function ){
	std::array<T,S> result{};
	for( size_t i{0}; i < S ; ++i ){
		result[i] = function( A[i], B[i] );
	}
	return result;
}

template< typename T, size_t S, typename Function, size_t... I >
constexpr std::array<T,S>
transform_tmpl_impl( std::array<T,S> const & A, std::array<T,S> const & B, Function function, std::index_sequence<I...> /*seq*/ ){
	return { function( A[I], B[I] )... };
}

template< typename T, size_t S, typename Function >
constexpr std::array<T,S>
transform_tmpl( std::array<T,S> const & A, std::array<T,S> const & B, Function function ){
	return transform_tmpl_impl(A,B,function,std::make_index_sequence<S>());
}

template<typename Type, size_t Size>
static void utils_transform( benchmark::State & state ){
	auto A = generate<Type, Size>();
	auto B = generate<Type, Size>();
	constexpr auto add = [](auto A, auto B){return A+B;};
	for( auto _ : state ){	// NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( array_utils::transform( A, B, add ) );
	}
}

template<typename Type, size_t Size>
static void template_transform( benchmark::State & state ){
	auto A = generate<Type, Size>();
	auto B = generate<Type, Size>();
	constexpr auto add = [](auto A, auto B){return A+B;};
	for( auto _ : state ){	// NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( transform_tmpl( A, B, add ) );
	}
}

template<typename Type, size_t Size>
static void loop_transform( benchmark::State & state ){
	auto A = generate<Type, Size>();
	auto B = generate<Type, Size>();
	constexpr auto add = [](auto A, auto B){return A+B;};
	for( auto _ : state ){	// NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( transform_loop( A, B, add ) );
	}
}

	BENCHMARK_TEMPLATE(    utils_transform, long double,       1 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE( template_transform, long double,       1 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(     loop_transform, long double,       1 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(    utils_transform, long double,      10 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE( template_transform, long double,      10 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(     loop_transform, long double,      10 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(    utils_transform, long double,     100 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE( template_transform, long double,     100 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(     loop_transform, long double,     100 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(    utils_transform, long double,    1000 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE( template_transform, long double,    1000 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(     loop_transform, long double,    1000 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(    utils_transform, long double,   10000 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE( template_transform, long double,   10000 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(     loop_transform, long double,   10000 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
//	BENCHMARK_TEMPLATE(    utils_transform, long double,  100000 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
//	BENCHMARK_TEMPLATE( template_transform, long double,  100000 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
	BENCHMARK_TEMPLATE(     loop_transform, long double,  100000 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


BENCHMARK_MAIN();
