#include "Matrix3D.hpp"
#include "Matrix_ostream.hpp"
#include "gtest.hpp"

// Basing correctness off https://en.wikipedia.org/wiki/Rotation_matrix
using namespace pxlatn;

class test_rotate : public ::testing::Test {
	public:
	const long double pi{ 3.1415926535897932384626433832795028841971693993751058209749445923078164062L };
	const double theta = pi / 3;
	template< typename Type, size_t row, size_t col >
	void test_eq( Matrix< Type, row, col > a, Matrix< Type, row, col > b ) {
		for( size_t y = 0; y < col; ++y ) {
			for( size_t x = 0; x < row; ++x ) {
				EXPECT_FLOAT_EQ( a.get( x, y ), b.get( x, y ) )
					<< "a:\n" << a << "\nb:\n" << b;
			}
		}
	};
};

gTEST_F( test_rotate, rotate2d_eq ) {
	const double s = std::sin( theta );
	const double c = std::cos( theta );
	Matrix< double, 2, 2 > raw_rotate{
		c,-s,
		s, c
	};
	auto new_rotate = rotate_2D< double >( theta );
	test_eq( raw_rotate, new_rotate );
}

gTEST_F( test_rotate, rotate_x_eq ) {
	const double s = std::sin( theta );
	const double c = std::cos( theta );
	Matrix< double, 3, 3 > raw_rotate_x{
		1, 0, 0,
		0, c,-s,
		0, s, c
	};
	auto new_rotate_x = rotate_x< double >( theta );
	test_eq( raw_rotate_x, new_rotate_x );
}

gTEST_F( test_rotate, rotate_y_eq ) {
	const double s = std::sin( theta );
	const double c = std::cos( theta );
	Matrix< double, 3, 3 > raw_rotate_y{
		 c, 0, s,
		 0, 1, 0,
		-s, 0, c,
	};
	auto new_rotate_y = rotate_y< double >( theta );
	test_eq( raw_rotate_y, new_rotate_y );
}

gTEST_F( test_rotate, rotate_z_eq ) {
	const double s = std::sin( theta );
	const double c = std::cos( theta );
	Matrix< double, 3, 3 > raw_rotate_z{
		c,-s, 0,
		s, c, 0,
		0, 0, 1
	};
	auto new_rotate_z = rotate_z< double >( theta );
	test_eq( raw_rotate_z, new_rotate_z );
}

gTEST_F( test_rotate, rotate2d_correctness ){
	Vector<double,2> V{1,0},A{0,1},B{-1,0},C{0,-1};
	auto rotate_90 = rotate_2D< double >( pi / 2 );
	test_eq( rotate_90 * V, A );
	test_eq( rotate_90 * A, B );
	test_eq( rotate_90 * B, C );
	test_eq( rotate_90 * C, V );
}

gTEST_F( test_rotate, rotate_x_correctness ){
	Vector<double,3> V{0,1,0},A{0,0,1},B{0,-1},C{0,0,-1};
	auto rotate_90 = rotate_x< double >( pi / 2 );
	test_eq( rotate_90 * V, A );
	test_eq( rotate_90 * A, B );
	test_eq( rotate_90 * B, C );
	test_eq( rotate_90 * C, V );
}

gTEST_F( test_rotate, rotate_y_correctness ){
	Vector<double,3> V{1,0,0},A{0,0,-1},B{-1,0,0},C{0,0,1};
	auto rotate_90 = rotate_y< double >( pi / 2 );
	test_eq( rotate_90 * V, A );
	test_eq( rotate_90 * A, B );
	test_eq( rotate_90 * B, C );
	test_eq( rotate_90 * C, V );
}

gTEST_F( test_rotate, rotate_z_correctness ){
	Vector<double,3> V{1,0,0},A{0,1,0},B{-1,0,0},C{0,-1,0};
	auto rotate_90 = rotate_z< double >( pi / 2 );
	test_eq( rotate_90 * V, A );
	test_eq( rotate_90 * A, B );
	test_eq( rotate_90 * B, C );
	test_eq( rotate_90 * C, V );
}

/// https://en.wikipedia.org/wiki/Rotation_matrix#Nested_dimensions
/// rotate_a should be equivalent of \f$ skew_symmeric(u) \sin \theta + (I - uu^T) \cos \theta + uu^T \f$
gTEST_F( test_rotate, arbitrary_axis ){
	const auto axis  = Vector < double, 3 >{ 1, 2, 3 }.unit();
	const auto I = Matrix< double, 3, 3 >::identity();
	const auto uuT = axis * axis.transpose();
	Matrix< double, 3, 3 > rotate_axis_raw =
		skew_symmetric( axis ) * std::sin( theta ) +
		( I - uuT ) * std::cos( theta ) +
		uuT;
	auto rotate_axis = rotate_a< double >( axis, theta );
	test_eq( rotate_axis, rotate_axis_raw );
}
