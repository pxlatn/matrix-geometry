#include "Matrix3D.hpp"
#include "Matrix_ostream.hpp"

using namespace pxlatn;

int main(){
	/// Matrix checks
	constexpr Matrix< int, 2,3 >
		A{	1, 2, 3,
			20,40,60
		},
		B{	4, 5, 6,
			80,100,120
		};
	constexpr Matrix< int, 3,2 >
		C{	4, 80,
			5,100,
			6,120
		};
	constexpr int k = 2;

	// Basic access
	static_assert( A[2] == 3);
	static_assert( A.get(1,1) == 40);
	// Operators (same as vector)
	static_assert( A+B == Matrix<int,2,3>{5,7,9,100,140,180});
	static_assert( B-A == Matrix<int,2,3>{3,3,3,60,60,60});
	static_assert( -B  == Matrix<int,2,3>{-4,-5,-6,-80,-100,-120});
	static_assert( A*k == Matrix<int,2,3>{2,4,6,40,80,120});
	static_assert( k*A == Matrix<int,2,3>{2,4,6,40,80,120});
	static_assert( B/k == Matrix<int,2,3>{2,2,3,40,50,60});
	// Matrix operators
	static_assert( B.transpose() == C);
	static_assert( Vector<int,2>(A.get_col(1)) == Matrix<int,2>{2,40});
	static_assert( Vector<int,3>(A.get_row(1)) == Matrix<int,3>{20,40,60});
	static_assert( A*C == Matrix<int,2,2>{32,640,640,12800});
	static_assert( C*A == Matrix<int,3,3>{1604,3208,4812,2005,4010,6015,2406,4812,7218});

	/// Vector operators
	constexpr Vector< int, 3 >
		vA{ 1, 0, 0 },
		vB{ 0, 1, 0 },
		vC{ 0, 0, 1 },
		vD = vA + vB + vC;
	static_assert( cross( vA, vB ) == vC);
	static_assert( vD.length_sq() == 3);
	static_assert( vD.invert() == Vector<int,3>{-1,-1,-1});
	static_assert( vD*8 == Vector<int,3>{8,8,8});

	/// Floats
	constexpr Matrix<double, 2,2>
		Af{	0.0, 2.5,
			3.0, 4.5 },
		Bf{	0.0, 5.0,
			6.0, 9.0 };
	static_assert( Af*2 == Bf);
	constexpr Matrix<double,1,1>
		Cf{ 0.1 },
		Df{ 0.2 },
		Ef{ 0.3 };
	static_assert( Cf+Df == Ef);

	/// Check of zero
	constexpr Matrix< int, 0,0 > zero{};
	// Operators (same as vector)
	static_assert( zero+zero == zero);
	static_assert( zero-zero == zero);
	static_assert( zero*2 == zero);
	static_assert( zero/2 == zero);
	// Matrix operators
	static_assert( zero.transpose() == zero);
	static_assert( zero*zero == zero);
	static_assert( zero*zero == zero);

	/// matrix setup
	constexpr Matrix<int, 2,3 > matrix_from_array{ std::array<int, 6>{1,2,3,4,5,6} };
}

// vim: filetype=cpp.doxygen
