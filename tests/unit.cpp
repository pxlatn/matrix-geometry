#include "Matrix.hpp"
#include "Matrix3D.hpp"
#include "Matrix_ostream.hpp"
#include "gtest.hpp"

using Type = double;
const Type pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062;
using namespace pxlatn;


gTEST( Matrix, construction ){
	Matrix<int, 3, 3> M{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	Matrix<int, 3, 3> A{ std::array<int, 9>{ 1, 2, 3, 4, 5, 6, 7, 8, 9 } };
	gEXPECT_EQ( M, A );
}

// operator| is a convenience reverse order operator*
gTEST( Matrix, pipe ){
	Vector<int,3>
		V {	2, 3, 4 };
	Matrix<int,3,3> M {
			0, 1000, 5,
			1,  100, 7,
			0,   10, 6
	};
	auto output1 = M * V;
	auto output2 = V | M;
	// just check that the two are equal
	gEXPECT_EQ( output1, output2 );
}

gTEST( Matrix2D, rotate ){
	Vector<Type,2> input{0,1}, target{-1,0};
	auto M = rotate_2D<Type>( pi/2 );
	auto output = M * input;
	gEXPECT_EQ( target, output );
}

gTEST( Matrix3D, rotate_x ){
	Vector<Type,3>
		input { 1,1,1 },
		target{ 1,-1,1 };
	auto M = rotate_x( pi/2 );
	auto output = M * input;
	gEXPECT_EQ( target, output );
}

gTEST( Matrix3D, rotate_y ){
	Vector<Type,3>
		input { 1,1,1 },
		target{ 1,1,-1 };
	auto M = rotate_y( pi/2 );
	auto output = M * input;
	gEXPECT_EQ( target, output );
}

gTEST( Matrix3D, rotate_z ){
	Vector<Type,3>
		input { 1,1,1 },
		target{ -1,1,1 };
	auto M = rotate_z( pi/2 );
	auto output = M * input;
	gEXPECT_EQ( target, output );
}

gTEST( Matrix3D, rotate_a ){
	Vector<Type,3>
		input	{ 1, 0, 0},
		axis	{ 0, 1, 0},
		target	{ 0, 0, 1};
	auto M = rotate_a( axis, -pi/2 );
	auto output = M * input;
	gEXPECT_EQ( target, output );
}

gTEST( Vector3D, cross ){
	Vector<Type,3>
		A{1,0,0},
		B{0,1,0},
		target{0,0,1},
		output = cross(A,B);
	gEXPECT_EQ( target, output );
}

gTEST( Matrix3D, skew_symmetric ){
	Vector<Type,3>
		A{1,0,0},
		B{0,1,0},
		target{0,0,1},
		o_cross = cross(A,B),
		o_skew  = skew_symmetric(A) * B;
	// Is the skew_symmetric function just the matrix form of a cross product of 3d vectors?
	// because this is much slower than cross()
	gEXPECT_EQ( o_cross, o_skew );
	gEXPECT_EQ( target, o_skew );
}

gTEST( Matrix3D, rotate_onto ){
	// Matrix3D rotate_onto( Vector3D a, Vector3D b )
	Vector<Type,3>
		start	{0,0,1},
		end		{0,-1,0};
	auto M = rotate_onto( start.unit(), end.unit() );
	auto M_t = M.transpose();
	gEXPECT_EQ( end,   M * start ) << M;
	gEXPECT_EQ( start, M_t * end ) << M;
}

gTEST( Vector, angle_between ){
	Vector<Type,3>
		X{1,0,0}, Y{0,1,0}, Z{0,0,1};
	auto ang_XY = angle_between( X, Y );
	auto ang_YX = angle_between( Y, X );
	auto ang_XZ = angle_between( X, Z );
	auto ang_ZX = angle_between( Z, X );
	auto ang_YZ = angle_between( Y, Z );
	auto ang_ZY = angle_between( Z, Y );
	gEXPECT_EQ( ang_XY, ang_YZ ); gEXPECT_EQ( ang_YZ, ang_ZX );
	gEXPECT_EQ( ang_ZX, ang_XY ); gEXPECT_EQ( ang_ZY, ang_YX );
	gEXPECT_EQ( ang_YX, ang_XZ ); gEXPECT_EQ( ang_XZ, ang_ZY );
}

gTEST( Vector, scatter ){
	Vector<Type,3>
		A{0,0,0},B{0,10,0},
		C{3,3,0};
	gEXPECT_EQ( 3, scatter(A,B, C) );
}

gTEST( Vector, reflect ){
	Vector<Type,3>
		in{-1,-1,-1},
		surface{0,0,1},
		out = reflect( in, surface ),
		target{-1,-1,1};
	gEXPECT_EQ( target, out );
}

gTEST( Vector, DISABLED_intersect ){
	ADD_FAILURE() << "Not tested";
	Vector<Type,3>
		target;
}

// vim: filetype=cpp.doxygen
