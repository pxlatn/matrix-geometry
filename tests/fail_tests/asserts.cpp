#include "Matrix.hpp"

using namespace pxlatn;

void incorrect_init(){
	Matrix<int,2,3> too_large{
		1, 2, 3,
		4, 5, 6,
		10
	};
}

void bad_multiplications(){
	Matrix<int, 2,3> rct{ 1,2,3,4,5,6 };
	Matrix<int, 3,3> sqr{ 1,2,3,4,5,6,7,8,9 };
	// should succeed
	auto rct_x_sqr = rct * sqr;
	// should fail
	auto sqr_x_rct = sqr * rct;
	//	friend constexpr _Tp dot( Matrix<_Tp,_Nrows,_Ncols> const & lhs, Matrix<_Tp,_Nrows,_Ncols> const & rhs ){
	//		static_assert( Matrix<_Tp,_Nrows,_Ncols>::is_vector, "A scalar product between Matrices is not defined");
//	auto dot = rct.dot(rct,rct);
}

void vector_ops(){
	Matrix<int, 2,3 > matrix{ 1,2,3,4,5,6 };
	//	constexpr _Tp length_sq() const {
	//		static_assert( is_vector, "Length only makes sense in the context of a vector." );
	matrix.length_sq();
	//	_Tp length() const {
	//		static_assert( is_vector, "Length only makes sense in the context of a vector." );
	matrix.length();
	//	void normalize(){
	//		static_assert( is_vector, "conversion to unit vector is only implemented for vectors." );
	matrix.normalize();
	//	Matrix unit() const {
	//		static_assert( is_vector, "conversion to unit vector is only implemented for vectors." );
	matrix.unit();
	//	void truncate( _Tp max ){
	//		static_assert( is_vector, "Length only makes sense in the context of a vector." );
	matrix.truncate( 5 );
	//	constexpr Matrix invert() const {
	//		static_assert( is_vector, "invert() only implemented for vectors." );
	matrix.invert();
}

int main(){
	incorrect_init();
	bad_multiplications();
	vector_ops();
}
