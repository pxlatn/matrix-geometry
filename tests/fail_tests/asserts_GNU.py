#!/usr/bin/env python3

import subprocess
import sys

process = subprocess.Popen(
    sys.argv[1:],
    stderr=subprocess.STDOUT,
    stdout=subprocess.PIPE
)
stdout, stderr = process.communicate(timeout=300)  # 300 seconds = 5 minutes
output = stdout.decode()

must_not_find = [
    "linker command failed",
    "Built target",
]
must_find = [
    "too many initializers",
    "Length only makes sense in the context of a vector.",
    "conversion to unit vector is only implemented for vectors.",
    "invert() only implemented for vectors.",
]
failures = {
    'must_not_find': [],
    'must_find': []
}

for fail_string in must_not_find:
    if fail_string in output:
        failures['must_not_find'].append(fail_string)

for pass_string in must_find:
    if pass_string not in output:
        failures['must_find'].append(pass_string)

if (not failures['must_not_find']) and (not failures['must_find']):
    sys.exit(0)

print(output)

for fail in must_not_find:
    if fail in failures['must_not_find']:
        print('\x1b[1;31m', 'ERROR: Found:    ', fail, '\x1b[0m')
    else:
        print('\x1b[1;32m', 'PASS:  Not Found:', fail, '\x1b[0m')

for fail in must_find:
    if fail in failures['must_find']:
        print('\x1b[1;33m', 'ERROR: Not Found:', fail, '\x1b[0m')
    else:
        print('\x1b[1;32m', 'PASS:  Found:    ', fail, '\x1b[0m')

sys.exit(1)
