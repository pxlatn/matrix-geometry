#include "Matrix3D.hpp"
#include <benchmark/benchmark.h>
#include <random>
#include <sstream>
#include <type_traits>

using namespace pxlatn;

using SCALAR = double;
using Matrix3D = Matrix<SCALAR,3,3>;
using Vector3D = Vector<SCALAR,3>;

#define gBENCHMARK( x ) BENCHMARK( x )  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)

// get random value for integral type
template<typename T, std::enable_if_t<std::is_integral<T>::value, int> = 0>
T get_rng() {
	static unsigned seed = std::random_device()();
	static std::default_random_engine engine{ seed };
	static std::mt19937 gen{ engine() };
	static std::uniform_int_distribution<T> dist{};
	return dist( gen );
}

// get random value for floating type
template<typename T, std::enable_if_t<std::is_floating_point<T>::value, int> = 0>
T get_rng() {
	static unsigned seed = std::random_device()();
	static std::default_random_engine engine{ seed };
	static std::mt19937 gen{ engine() };
	static std::uniform_real_distribution<T> dist{};
	return dist( gen );
}

// generate array filled with random values
template<typename T, size_t S>
std::array<T,S> generate(){
	std::array<T,S> result{};
	for( T & c : result ){
		c = get_rng<T>();
	}
	return result;
}

/// array_utils

template<size_t Size>
static void utils_dot( benchmark::State & state ){
	auto A = generate<long double, Size>();
	auto B = generate<long double, Size>();
	for( auto _ : state ){	// NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( array_utils::dot( A, B ) );
	}
}
BENCHMARK_TEMPLATE( utils_dot, 8 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)

template<size_t Size>
static void utils_transform( benchmark::State & state ){
	auto A = generate<long double, Size>();
	auto B = generate<long double, Size>();
	auto add = []( auto a, auto b ){ return a+b; };
	for( auto _ : state ){	// NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( array_utils::transform( A, B, add ) );
	}
}
BENCHMARK_TEMPLATE( utils_transform, 8 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


/// Constructors

template<size_t Size>
static void matrix_default( benchmark::State & state ){
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( Matrix<SCALAR,Size,Size>{} );
	}
}
BENCHMARK_TEMPLATE( matrix_default, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


static void matrix_construct( benchmark::State & state ){
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( Matrix3D{ 1, 2, 3, 4, 5, 6, 7, 8, 9 } );
	}
}
gBENCHMARK( matrix_construct );


template<size_t Size>
static void matrix_from_array( benchmark::State & state ){
	auto test_arr = generate<SCALAR,Size*Size>();
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			Matrix<SCALAR, Size, Size>{ test_arr }
		);
	}
}
BENCHMARK_TEMPLATE( matrix_from_array, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_from_copy( benchmark::State & state ){
	Matrix<SCALAR, Size, Size> matrix{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			Matrix<SCALAR, Size, Size>{ matrix }
		);
	}
}
BENCHMARK_TEMPLATE( matrix_from_copy, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)



/// Member access

template<size_t Size>
static void matrix_get_index( benchmark::State & state ){
	Matrix<SCALAR, Size, Size> matrix{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		for( size_t i=0 ; i<Size*Size ; ++i ){
			benchmark::DoNotOptimize( matrix[i] );
		}
	}
}
BENCHMARK_TEMPLATE( matrix_get_index, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_get_coord( benchmark::State & state ){
	Matrix<SCALAR, Size, Size> matrix{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		for( size_t y=0 ; y<Size ; ++y ){
			for( size_t x=0 ; x<Size ; ++x ){
				benchmark::DoNotOptimize(
					matrix.get(x,y)
				);
			}
		}
	}
}
BENCHMARK_TEMPLATE( matrix_get_coord, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)



/// Matrix Operations

template<size_t A, size_t B>
static void matrix_transpose( benchmark::State & state ){
	Matrix<SCALAR, A, B> matrix_1st{ generate<SCALAR, A*B>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( matrix_1st.transpose() );
	}
}
BENCHMARK_TEMPLATE( matrix_transpose, 4,8 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


static void matrix_submatrix( benchmark::State & state ){
	Matrix<int,4,4> M{ generate<int,16>() };
	for( auto _ : state ){	// NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( M.submatrix<3,3>( {1,2,3}, {0,1,3} ) );
	}
}
gBENCHMARK( matrix_submatrix );


template<size_t Size>
static void matrix_determinant( benchmark::State & state ){
	Matrix<int,Size,Size> M{ generate<int, Size*Size>() };
	for( auto _ : state ){	// NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize( M.determinant() );
	}
}
BENCHMARK_TEMPLATE( matrix_determinant,  1 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_determinant,  2 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_determinant,  3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_determinant,  4 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_determinant,  5 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_determinant,  6 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_determinant,  7 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_determinant,  8 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_determinant,  9 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_determinant, 10 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)

template<size_t S>
constexpr std::array<size_t,S> arr_range() {
	std::array<size_t,S> result;
	for( size_t i{0}; i<S ; ++i )
		result[i] = i;
	return result;
}


/// Vector Operations

template<size_t Size>
static void vector_length_sq( benchmark::State & state ){
	Vector<SCALAR,Size> vector{ generate<SCALAR,Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			vector.length_sq()
		);
	}
}
BENCHMARK_TEMPLATE( vector_length_sq, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void vector_length( benchmark::State & state ){
	Vector<SCALAR,Size> vector{ generate<SCALAR,Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			vector.length()
		);
	}
}
BENCHMARK_TEMPLATE( vector_length, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void vector_unit( benchmark::State & state ){
	Vector<SCALAR,Size> vector{ generate<SCALAR,Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			vector.unit()
		);
	}
}
BENCHMARK_TEMPLATE( vector_unit, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void vector_trunc( benchmark::State & state ){
	Vector<SCALAR,Size> vector{ generate<SCALAR,Size>() };
	for( auto _ : state ) {  // NOLINT(clang-analyzer-deadcode.DeadStores)
		vector.truncate( 10 );
	}
	benchmark::DoNotOptimize( vector );
}
BENCHMARK_TEMPLATE( vector_trunc, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void vector_invert( benchmark::State & state ){
	Vector<SCALAR,Size> vector{ generate<SCALAR,Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			vector.invert()
		);
	}
}
BENCHMARK_TEMPLATE( vector_invert, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)

static void vector_dot( benchmark::State & state ){
	Vector3D vector1{0,1,0};
	Vector3D vector2{1,0,0};
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			dot( vector1, vector2 )
		);
	}
}
gBENCHMARK( vector_dot );



/// Matrix Maths

template<size_t Size>
static void matrix_add_to( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix{   generate<SCALAR,Size*Size>() };
	Matrix<SCALAR,Size,Size> matrix_d{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		matrix += matrix_d;
	}
	benchmark::DoNotOptimize(matrix);
}
BENCHMARK_TEMPLATE( matrix_add_to, 2 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_sub_from( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix{   generate<SCALAR,Size*Size>() };
	Matrix<SCALAR,Size,Size> matrix_d{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		matrix -= matrix_d;
	}
	benchmark::DoNotOptimize(matrix);
}
BENCHMARK_TEMPLATE( matrix_sub_from, 2 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_mul_by( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		matrix *= 2;
	}
	benchmark::DoNotOptimize(matrix);
}
BENCHMARK_TEMPLATE( matrix_mul_by, 2 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_div_by( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		matrix /= 2;
	}
	benchmark::DoNotOptimize(matrix);
}
BENCHMARK_TEMPLATE( matrix_div_by, 2 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)

template<size_t Size>
static void matrix_add( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix{   generate<SCALAR,Size*Size>() };
	Matrix<SCALAR,Size,Size> matrix_d{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		matrix + matrix_d;
	}
	benchmark::DoNotOptimize(matrix);
}
BENCHMARK_TEMPLATE( matrix_add, 2 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_sub( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix{   generate<SCALAR,Size*Size>() };
	Matrix<SCALAR,Size,Size> matrix_d{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		matrix - matrix_d;
	}
	benchmark::DoNotOptimize(matrix);
}
BENCHMARK_TEMPLATE( matrix_sub, 2 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_mul_scalar( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		matrix * 2;
	}
	benchmark::DoNotOptimize(matrix);
}
BENCHMARK_TEMPLATE( matrix_mul_scalar, 2 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_div_scalar( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		matrix / 2;
	}
	benchmark::DoNotOptimize(matrix);
}
BENCHMARK_TEMPLATE( matrix_div_scalar, 2 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template< typename _Typ, size_t A, size_t B, size_t C=A >
static void matrix_mul_matrix( benchmark::State & state ){
	Matrix<_Typ,A,B> matrix1{ generate<_Typ,A*B>() };
	Matrix<_Typ,B,C> matrix2{ generate<_Typ,B*C>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			matrix1 * matrix2
		);
	}
	std::ostringstream os;
	os << A << 'x' << C;
	state.SetLabel( os.str() );
}

BENCHMARK_TEMPLATE( matrix_mul_matrix, double ,3,3,1 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)
BENCHMARK_TEMPLATE( matrix_mul_matrix, double ,3,3,3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_eq( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix1{ generate<SCALAR,Size*Size>() };
	Matrix<SCALAR,Size,Size> matrix2{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			matrix1 == matrix2
		);
	}
}
BENCHMARK_TEMPLATE( matrix_eq, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)


template<size_t Size>
static void matrix_neq( benchmark::State & state ){
	Matrix<SCALAR,Size,Size> matrix1{ generate<SCALAR,Size*Size>() };
	Matrix<SCALAR,Size,Size> matrix2{ generate<SCALAR,Size*Size>() };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			matrix1 != matrix2
		);
	}
}
BENCHMARK_TEMPLATE( matrix_neq, 3 );  // NOLINT(cert-err58-cpp,cppcoreguidelines-owning-memory)



/// Matrix3D

static void matrix3d_pipe_vector( benchmark::State & state ){
	Vector3D vector;
	Matrix3D matrix;
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		vector |= matrix;
	}
	benchmark::DoNotOptimize( vector );
}
gBENCHMARK( matrix3d_pipe_vector );


static void matrix3d_rotate2d( benchmark::State & state ){
	for( auto _ : state ) {  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			rotate_2D( 5.0 )
		);
	}
}
gBENCHMARK( matrix3d_rotate2d );


static void matrix3d_rotate_x( benchmark::State & state ){
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			rotate_x( 5.0 )
		);
	}
}
gBENCHMARK( matrix3d_rotate_x );


static void matrix3d_rotate_y( benchmark::State & state ){
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			rotate_y( 5.0 )
		);
	}
}
gBENCHMARK( matrix3d_rotate_y );


static void matrix3d_rotate_z( benchmark::State & state ){
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			rotate_z( 5.0 )
		);
	}
}
gBENCHMARK( matrix3d_rotate_z );


static void matrix3d_rotate_a( benchmark::State & state ){
	Vector3D axis{ 1,1,0 };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			rotate_a( axis, 5.0 )
		);
	}
}
gBENCHMARK( matrix3d_rotate_a );


static void matrix3d_cross( benchmark::State & state ){
	Vector3D A{1,0,0}, B{0,1,0};
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			cross( A, B )
		);
	}
}
gBENCHMARK( matrix3d_cross );


static void matrix3d_skew_cross( benchmark::State & state ){
	Vector3D A{1,0,0}, B{0,1,0};
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			skew_symmetric(A) * B
		);
	}
}
gBENCHMARK( matrix3d_skew_cross );


static void matrix3d_skew_symmetric( benchmark::State & state ){
	Vector3D vector{ 1,1,0 };
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			skew_symmetric( vector )
		);
	}
}
gBENCHMARK( matrix3d_skew_symmetric );


static void matrix3d_rotate_onto( benchmark::State & state ){
	Vector3D A{1,1,1}, B{0,1,0};
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			rotate_onto( A, B )
		);
	}
}
gBENCHMARK( matrix3d_rotate_onto );


static void matrix3d_angle_between( benchmark::State & state ){
	Vector3D A{1,1,1}, B{0,1,0};
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			angle_between( A, B )
		);
	}
}
gBENCHMARK( matrix3d_angle_between );


static void matrix3d_scatter( benchmark::State & state ){
	Vector3D
		A{0,1,0}, B{1,0,0},
		target{0,0,1};
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			scatter( A, B, target )
		);
	}
}
gBENCHMARK( matrix3d_scatter );


static void matrix3d_reflect( benchmark::State & state ){
	Vector3D A{0,1,0},B{0,1,1};
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			reflect( A, B )
		);
	}
}
gBENCHMARK( matrix3d_reflect );


static void matrix3d_intersect( benchmark::State & state ){
	Vector3D
		A1{-1,0,0}, A2{1,0,0},
		B1{0,-1,0}, B2{0,1,0};
	for( auto _ : state ){  // NOLINT(clang-analyzer-deadcode.DeadStores)
		benchmark::DoNotOptimize(
			intersect( A1, A2, B1, B2 )
		);
	}
}
gBENCHMARK( matrix3d_intersect );



BENCHMARK_MAIN();
