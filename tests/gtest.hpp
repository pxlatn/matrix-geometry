#include <gtest/gtest.h>

/// Silence certain clang-tidy checks regarding these specific macros

#define gTEST( x, y )   TEST( x, y )   // NOLINT(hicpp-special-member-functions,cert-err58-cpp,cppcoreguidelines-owning-memory)
#define gTEST_F( x, y ) TEST_F( x, y ) // NOLINT(hicpp-special-member-functions,cert-err58-cpp,cppcoreguidelines-owning-memory)

#define gEXPECT_EQ( x, y ) EXPECT_EQ( x, y ) // NOLINT(cppcoreguidelines-pro-type-vararg)
#define gASSERT_EQ( x, y ) ASSERT_EQ( x, y ) // NOLINT(cppcoreguidelines-pro-type-vararg)

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
