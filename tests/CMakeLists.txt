add_subdirectory( fail_tests )

# simple library, adds NOLINT comments to defines
add_library( gtest INTERFACE ) # gtest.hpp
target_link_libraries( gtest INTERFACE
	GTest::GTest
)

add_executable( random_tests random.cpp )
target_link_libraries( random_tests
	gtest
	matrix
	${ASAN}
)
target_compile_options( random_tests PRIVATE ${ASAN} )
add_test( random_tests random_tests )

add_executable( wiki_tests wiki.cpp )
target_link_libraries( wiki_tests
	gtest
	matrix
	${ASAN}
)
target_compile_options( wiki_tests PRIVATE ${ASAN} )
add_test( wiki_operations wiki_tests )

add_executable( constexpr_tests constexpr.cpp )
target_link_libraries( constexpr_tests
	matrix
	${ASAN}
)
target_compile_options( constexpr_tests PRIVATE ${ASAN} )
add_test( assert_constexpr constexpr_tests )

add_executable( unit_tests unit.cpp )
target_link_libraries( unit_tests
	gtest
	matrix
	${ASAN}
)
target_compile_options( unit_tests PRIVATE ${ASAN} )
add_test( unit_tests unit_tests )

add_executable( rotation_tests rotate.cpp )
target_link_libraries( rotation_tests
	gtest
	matrix
	${ASAN}
)
target_compile_options( rotation_tests PRIVATE ${ASAN} )
# Disabled until a better way of testing this is available
#add_test( rotation_tests rotation_tests )

find_package( benchmark )
if( benchmark_FOUND )
	add_executable( benchmarks benchmark.cpp )
	target_link_libraries( benchmarks
		PRIVATE
		matrix
		benchmark::benchmark
	)
	add_test(
		NAME    benchmarking
		COMMAND benchmarks
			--benchmark_counters_tabular=true
		CONFIGURATIONS bench
	)
	# compare definitions of transform
	add_executable( bench-transform bench-transform.cpp )
	target_link_libraries( bench-transform
		PRIVATE
		matrix
		benchmark::benchmark
	)
else()
	message( WARNING "Google Benchmark not found, not building benchmark tests." )
endif( benchmark_FOUND )
