#include "Matrix.hpp"
#include "Matrix3D.hpp"
#include "Matrix_ostream.hpp"
#include "gtest.hpp"

/*! @section Wiki tests
 *	@brief Test cases <strike>stolen</strike> copied from wikipedia
 */

using namespace pxlatn;

gTEST( wiki, add ){ // Addition
	Matrix<int,2,3>
		A {	1, 3, 1,
			1, 0, 0	},
		B {	0, 0, 5,
			7, 5, 0 },
		output = A + B,
		target {
			1, 3, 6,
			8, 5, 0
		};
	gEXPECT_EQ( target, output );
}

gTEST( wiki, scalar_mul ){ // Scalar Multiplication
	Matrix<int,2,3>
		input {
			1,  8, -3,
			4, -2,  5
		},
		output = input * 2,
		target {
			2, 16, -6,
			8, -4, 10
		};
	gEXPECT_EQ( target, output );
}

gTEST( wiki, transpose ){ // Transposition
	Matrix<int,2,3>
		input {
			1, 2, 3,
			4, 5, 6
		};
	Matrix<int,3,2>
		output = input.transpose(),
		target {
			1, 4,
			2, 5,
			3, 6
		};
	gEXPECT_EQ( target, output );
}

gTEST( wiki, matrix_mul ){ // Matrix Multiplication
	Matrix<int,2,3>
		A {	2, 3, 4,
			1, 0, 0	};
	Matrix<int,3,2>
		B {	0, 1000,
			1,  100,
			0,   10	};
	Matrix<int,2,2>
		output = A * B,
		target {
			3, 2340,
			0, 1000
		};
	gEXPECT_EQ( target, output );
	Matrix<int,2,2>
		E{ 1, 2, 3, 4 },
		F{ 0, 1, 0, 0 },
		G{ 0, 1, 0, 3 },
		H{ 3, 4, 0, 0 };
	gEXPECT_EQ( E*F, G );
	gEXPECT_EQ( F*E, H );
}

/* Tests of edge cases */

gTEST( edge, zero ){
	Matrix< int, 0,0 > zero{};
	// Operators (same as vector)
	gEXPECT_EQ( zero, zero+zero );
	gEXPECT_EQ( zero, zero-zero );
	gEXPECT_EQ( zero, zero*2 );
	gEXPECT_EQ( zero, zero/2 );
	// Matrix operators
	gEXPECT_EQ( zero, zero.transpose() );
	gEXPECT_EQ( zero, zero*zero );
}

gTEST( edge, exceptions ){
	Matrix< int, 2,3 >
		A{ 1,2,3,4,5,6 },
		zero{};
	gEXPECT_EQ( A * 0, zero );
	try{
		A / 0;
		ADD_FAILURE() << "Didn't throw";
	} catch( std::overflow_error & err ){
		SUCCEED();
	}
	EXPECT_THROW( A / 0, std::overflow_error );
}

/*
 * Matrix tests
 * https://en.wikipedia.org/wiki/Matrix_(mathematics)
 */

class matrix_oper : public ::testing::Test {
	protected:
	Matrix< int, 2,3 >
		A{	1, 2, 3,
			20,40,60
		},
		B{	4, 5, 6,
			80,100,120
		};
	Matrix< int, 3,2 >
		C{	4, 80,
			5,100,
			6,120
		};
	int k = 2;
};

gTEST_F( matrix_oper, matrix_add ){
	Matrix<int,2,3> target{5,7,9,100,140,180};
	gEXPECT_EQ( A + B, target );
	A += B;
	gEXPECT_EQ( A, target );
}

gTEST_F( matrix_oper, matrix_sub ){
	Matrix<int,2,3> target{3,3,3,60,60,60};
	gEXPECT_EQ( B - A, target );
	B -= A;
	gEXPECT_EQ( B, target );
}

gTEST_F( matrix_oper, scalar_mul ){
	Matrix<int,2,3> target{2,4,6,40,80,120};
	gEXPECT_EQ( A * k, target );
	A *= k;
	gEXPECT_EQ( A, target );
}

gTEST_F( matrix_oper, scalar_div ){
	Matrix<int,2,3> target{2,2,3,40,50,60};
	gEXPECT_EQ( B / k, target );
	B /= k;
	gEXPECT_EQ( B, target );
}

gTEST_F( matrix_oper, transpose ){
	gEXPECT_EQ( C, B.transpose() );
}

gTEST_F( matrix_oper, matrix_mul ){
	Matrix<int,2,2> target_AC{32,640,640,12800};
	Matrix<int,3,3> target_CA{1604,3208,4812,2005,4010,6015,2406,4812,7218};
	gEXPECT_EQ( target_AC, A * C );
	gEXPECT_EQ( target_CA, C * A );
}

gTEST_F( matrix_oper, get_col ){
	std::array<int,2> target{2,40};
	gEXPECT_EQ( A.get_col(1), target );
}

gTEST_F( matrix_oper, get_row ){
	std::array<int,3> target{20,40,60};
	gEXPECT_EQ( A.get_row(1), target );
}

gTEST_F( matrix_oper, submatrix ){
	Matrix<int,3,4> A{
		1,2,3,4,
		5,6,7,8,
		9,10,11,12
	};
	Matrix<int,2,3> target{1,3,4,5,7,8};
	gEXPECT_EQ(
		(A.submatrix<2,3>(
			{0,1},
			{0,2,3}
		)),
		target
	);
}

gTEST_F( matrix_oper, determinant ){
	constexpr int
		a =  2, b =  3, c =  5, d =  7,
		e = 11, f = 13, g = 17, h = 19,
		i = 23, j = 29, k = 31, l = 37,
		m = 41, n = 43, o = 47, p = 53;
	constexpr Matrix<int, 3, 3> M{
		a, b, c,
		d, e, f,
		g, h, i
	};
	constexpr int target = a*e*i - a*f*h + b*f*g - b*d*i + c*d*h - c*e*g;
	constexpr auto calc = M.determinant();
	gEXPECT_EQ( calc, target ) << M;
}

/*
 * Vector tests
 * https://en.wikipedia.org/wiki/Euclidean_vector
 */

class vector_oper : public ::testing::Test {
	protected:
		Vector< int, 3 >
			A{ 2,0,0 },
			B{ 0,3,0 },
			C{ 0,0,4 };
};

gTEST_F( vector_oper, cross ){
	Vector< int, 3 >
		result_AB{0,0,6},
		result_BA{0,0,-6};
	gEXPECT_EQ( cross( A, B ), result_AB );
	gEXPECT_EQ( cross( B, A ), result_BA );
}

gTEST_F( vector_oper, length ){
	Vector< int, 3 > hypot = B + C;
	gEXPECT_EQ( hypot.length_sq(), 25 );
	gEXPECT_EQ( hypot.length(), 5 );
}

gTEST_F( vector_oper, invert ){
	Vector< int, 3 > result{ -2,-3,-4 };
	gEXPECT_EQ( (A+B+C).invert(), result );
}

// vim: filetype=cpp.doxygen
